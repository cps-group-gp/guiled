#include "LedControl.h"
#include "binary.h"
#include <Wire.h>

# define I2C_SLAVE_ADDRESS 14

// DIN connects to pin 51
// CLK connects to pin 52
// CS connects to pin 22 

LedControl lc=LedControl(51,52,22,1);

volatile byte row_bytes[8];
volatile byte read_row_bytes[8];
int j = 0;
int ready_to_read = 0;

void setup() {
  Wire.begin(I2C_SLAVE_ADDRESS);
  Wire.setClock(50000);
  Wire.onReceive(receiveEvent);
  
//  Serial.begin(9600);  // initialing Serial with 9600 baud rate optional
  lc.shutdown(0,false);
  // Set brightness to a medium value
  lc.setIntensity(0,8);
  // Clear the display
  lc.clearDisplay(0);

  pinMode(18, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(18), drawFrame, RISING);

}

void receiveEvent(int howMany)
{
  int avail = Wire.available();
  if(j >= 8)
  {
    j = 0;
  }
  while(0 < avail) // loop through all but the last
  {
    row_bytes[j] = Wire.read();
    j++;
   
    avail = Wire.available();
  }

  if (j == 8)
  {
    for(int z = 0; z < 8; z++)
    {
      read_row_bytes[z] = row_bytes[z];
    }
    ready_to_read = 1;
  }
  
}

void drawFrame(){
  if(ready_to_read == 1)
  {
    for (int r = 0; r < j; r++)
    {
      lc.setRow(0, r, read_row_bytes[r]);
    }
    ready_to_read = 0;
  }
  
}

void loop() {
  
//  delay(15);

}
