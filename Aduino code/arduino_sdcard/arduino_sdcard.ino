//#include <I2C.h>

#include <tinyFAT.h>    // Sd card Library
#include <Wire.h>

# define I2C_SLAVE1_ADDRESS 11
# define I2C_SLAVE2_ADDRESS 12
# define I2C_SLAVE3_ADDRESS 13
# define I2C_SLAVE4_ADDRESS 14

//# define TRANSMISSION_DELAY 150

#define SCL 21
#define SDA 20

byte mmcCheck_response;  // storing response of mmc initializing function
byte openFile_response;  // storing response of open file function

char frame_number_char[10];
int frame_number = 0;
char frame_delay_char[10];
int frame_delay = 0;
char slave_count_char[10];
int slave_count = 0;
char rows[8][8];
byte row_bytes[8];
int startTime, endTime, computationTime = 0;
int final_frame_delay = 0;

byte slaveAddresses[4] = {11, 12, 13, 14};


void setup()  
{
  Wire.begin();
  Wire.setClock(50000);
  
//  Serial.begin(9600);  // initialing Serial with 9600 baud rate optional

  pinMode(33, OUTPUT);
  
  mmcCheck_response = file.initFAT(SPISPEED_VERYHIGH); // initializing MMC card
  if (  mmcCheck_response != NO_ERROR)  // check  MMC properly initialized 
  {    
//    Serial.println("Error Comes while Initializing the MMC card");
    while (true) {};
  }
//  Serial.println("MMC Initialized");

  openFile_response = file.openFile("ball.txt", FILEMODE_TEXT_READ);  // open the file in mmc card with name button.csv
  if (openFile_response == NO_ERROR)
  {
    file.readLn(frame_number_char, 10); 
    frame_number = atoi(frame_number_char);
    
    file.readLn(frame_delay_char, 10); 
    frame_delay = atoi(frame_delay_char);
    
    file.readLn(slave_count_char, 10); 
    slave_count = atoi(slave_count_char);
    
  }

//  Serial.println(millis());
}

int transmit(int slaveAddr)
{
  Wire.beginTransmission(slaveAddr);
  int errWrite1 = Wire.write((byte*)row_bytes, 8);
  int err1_1 = Wire.endTransmission();  

//  delay(50);
  return err1_1;
}

byte charArrayToByte(char* inputRow)
{
  int rowInt = 0;
  for(int q = 0; q < 8; q++)
  {
    rowInt = rowInt * 2 + (inputRow[q] - '0');
  } 
  return byte(rowInt);
}

void readFrameFromFile()
{
  for (int k = 0; k < 8; k++)
  {
    file.readLn(rows[k], 8);
    row_bytes[k] = charArrayToByte(rows[k]);  
  }
}

int I2C_ClearBus() 
{
  #if defined(TWCR) && defined(TWEN)
    TWCR &= ~(_BV(TWEN)); //Disable the Atmel 2-Wire interface so we can control the SDA and SCL pins directly
  #endif
  pinMode(SDA, INPUT_PULLUP); // Make SDA (data) and SCL (clock) pins Inputs with pullup.
  pinMode(SCL, INPUT_PULLUP);

//  delay(500);  // Wait 2.5 secs. This is strictly only necessary on the first power
  // up of the DS3231 module to allow it to initialize properly,
  // but is also assists in reliable programming of FioV3 boards as it gives the
  // IDE a chance to start uploaded the program
  // before existing sketch confuses the IDE by sending Serial data.

  boolean SCL_LOW = (digitalRead(SCL) == LOW); // Check is SCL is Low.
  if (SCL_LOW) { //If it is held low Arduino cannot become the I2C master. 
    return 1; //I2C bus error. Could not clear SCL clock line held low
  }

  boolean SDA_LOW = (digitalRead(SDA) == LOW);  // vi. Check SDA input.
  int clockCount = 20; // > 2x9 clock


  while (SDA_LOW && (clockCount > 0)) { //  vii. If SDA is Low,
    clockCount--;
  // Note: I2C bus is open collector so do NOT drive SCL or SDA high.
    pinMode(SCL, INPUT); // release SCL pullup so that when made output it will be LOW
    pinMode(SCL, OUTPUT); // then clock SCL Low
    delayMicroseconds(10); //  for >5uS
    pinMode(SCL, INPUT); // release SCL LOW
    pinMode(SCL, INPUT_PULLUP); // turn on pullup resistors again
    // do not force high as slave may be holding it low for clock stretching.
    delayMicroseconds(10); //  for >5uS
    // The >5uS is so that even the slowest I2C devices are handled.
    SCL_LOW = (digitalRead(SCL) == LOW); // Check if SCL is Low.
    int counter = 20;
    while (SCL_LOW && (counter > 0)) {  //  loop waiting for SCL to become High only wait 2sec.
      counter--;
//      delay(100);
      SCL_LOW = (digitalRead(SCL) == LOW);
    }
    if (SCL_LOW) { // still low after 2 sec error
      return 2; // I2C bus error. Could not clear. SCL clock line held low by slave clock stretch for >2sec
    }
    SDA_LOW = (digitalRead(SDA) == LOW); //   and check SDA input again and loop
  }
  if (SDA_LOW) { // still low
    return 3; // I2C bus error. Could not clear. SDA data line held low
  }


  // else pull SDA line low for Start or Repeated Start
  pinMode(SDA, INPUT); // remove pullup.
  pinMode(SDA, OUTPUT);  // and then make it LOW i.e. send an I2C Start or Repeated start control.
  // When there is only one I2C master a Start or Repeat Start has the same function as a Stop and clears the bus.
  /// A Repeat Start is a Start occurring after a Start with no intervening Stop.
  delayMicroseconds(10); // wait >5uS
  pinMode(SDA, INPUT); // remove output low
  pinMode(SDA, INPUT_PULLUP); // and make SDA high i.e. send I2C STOP control.
  delayMicroseconds(10); // x. wait >5uS
  pinMode(SDA, INPUT); // and reset pins as tri-state inputs which is the default state on reset
  pinMode(SCL, INPUT);
  return 0; // all ok
}


void loop() 
{
  startTime = millis();
  if(frame_number > 0)
  {
    digitalWrite(33, LOW);
    
    for(int c = 0; c < slave_count; c++)
    {
      readFrameFromFile();
      int err_1 = transmit(slaveAddresses[c]);
      if(err_1 != 0)
      {
        int rtn = I2C_ClearBus(); // clear the I2C bus first before calling Wire.begin()
        if (rtn != 0) 
        {
//          Serial.println(F("I2C bus error. Could not clear"));
        } 
        else 
        { 
          // bus clear
          // re-enable Wire
          // now can start Wire Arduino master
          Wire.begin();
          transmit(slaveAddresses[c]);
        }  
      }
    }

//    Serial.println(millis());
    digitalWrite(33, HIGH);
    delay(5);
    endTime = millis();
    computationTime = endTime - startTime;
   
  }
  
  else if(frame_number <= 0)
  {
    file.closeFile();
  }

//  Serial.println(computationTime);
  final_frame_delay = frame_delay - computationTime;
  frame_number--;
  delay(final_frame_delay);
  
}
